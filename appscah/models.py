from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class WRF(models.Model):
    domain    = models.CharField(max_length=50)
    parameter = models.CharField(max_length=50)
    time      = models.IntegerField(default=-2)
    path      = models.CharField(max_length=300)
    photo     = models.ImageField(upload_to='wrf')

    def __str__(self):
        informacion = self.domain+'/'+self.parameter+'/'+str(self.time)
        return informacion

