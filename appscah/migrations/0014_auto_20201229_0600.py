# Generated by Django 3.1.4 on 2020-12-29 11:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appscah', '0013_wrf_path'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wrf',
            name='path',
            field=models.CharField(max_length=300),
        ),
    ]
