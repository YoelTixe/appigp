# Generated by Django 3.1.4 on 2020-12-29 09:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appscah', '0012_auto_20201229_0353'),
    ]

    operations = [
        migrations.AddField(
            model_name='wrf',
            name='path',
            field=models.CharField(default='wer', max_length=200),
            preserve_default=False,
        ),
    ]
