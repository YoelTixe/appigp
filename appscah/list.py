#for root, dirs, files in os.walk("static/wrf"):
#    for filename in files:
#        print(filename)
#        print(root)

import os
from django.conf import settings
from appscah.models import WRF
from django.core.files import File
from urllib import request


for root, dirs, files in os.walk(os.path.join(settings.MEDIA_ROOT, 'wrf/SALIDAS_d01')):
    for filename in files:
        #photo = WRF(photo=root+'/'+filename)
        #photo.save()
        #response = requests.get(root+'/'+filename, stream=True)
        #result = request.urlretrieve(root+'/'+filename)
        #photo = WRF.save(
        #    os.path.basename(root+'/'+filename),
        #    File(open(result[0]))
        #)
        #photo.save()
        q = WRF(path=root+'/'+filename)
        with open(root+'/'+filename, 'rb') as f:   # use 'rb' mode for python3
            data = File(f)
            q.photo.save(filename, data, True)
            q.save()
            print(root+'/'+filename)

Dominios = [('Dominio 1 - Peru','wrf/SALIDAS_d01'),
    ('Dominio 2 - Centro del Peru','SALIDAS_d02'),
    ('Dominio 3 - Cuenca del Mantaro','SALIDAS_d03'),
    ('Dominio 4 - Sudamerica','SALIDAS_dSA')]
for nombre, ruta in Dominios:
    wrfs_path = WRF.objects.filter(path__contains=ruta)
    wrfs_path2 = wrfs_path.exclude(path__contains='SALIDAS_dSA')
    for wrf in wrfs_path2:
        wrf.domain = nombre
        wrf.save()
        print(nombre)

wrfs_path = WRF.objects.filter(path__contains='SALIDAS_d01')
wrfs_path2 = wrfs_path.exclude(path__contains='SALIDAS_dSA')
wrfs_path2.domain = 'Dominio 1 - Peru'
wrfs_path2.save()

wrfs_path3 = WRF.objects.filter(path__contains='SALIDAS_dSA')
wrfs_path3.domain = 'Dominio 4 - Sudamerica'
wrfs_path3.save()


Variables = [('Precipitacion Granizo (mm)','GRANIZO'),
    ('Precipitacion Nieve (mm)','NIEVE'),
    ('Perfil de Temperatura y Punto de Rocio','TEMP_PR'),
    ('Precipitacion (mm)','PRECIPITACIONES/Precipitaciones'),
    ('Acumulado de precipitacion (mm)','PRECIPITACIONES/ACUMULADO'),
    ('Humedad relativa media (%)','RH/HRM700-400'),
    ('Temperatura en superficie (gC)','TEMPERATURAS'),
    ('Direccion y velocidad del viento en superficie (m/s)','VIENTO/F_VIENTO'),
    ('Barbas de viento y div. en superficie','VIENTO/viento'),
    ('Direccion y velocidad del viento en 200hPa (m/s)','VIENTO/200/F_VIENTO'),
    ('Direccion y velocidad del viento en 500hPa (m/s)','VIENTO/500/F_VIENTO'),
    ('Direccion y velocidad del viento en 700hPa (m/s)','VIENTO/700/F_VIENTO'),
    ('Direccion y velocidad del viento en 850hPa (m/s)','VIENTO/850/F_VIENTO'),
    ('Lineas de corriente y div. 200hPa','STREAM/200'),
    ('Lineas de corriente y div. 500hPa','STREAM/500'),
    ('Lineas de corriente y div. 700hPa','STREAM/700'),
    ('Lineas de corriente y div. 850hPa','STREAM/850'),
    ('Lineas de corriente y div. 500hPa','1/VIENTO/STREAM/Stream'),
    ('Vector de viento y div. 500hPa','1/VIENTO/VECTOR/500'),
    ('Vector de viento y div. 700hPa','1/VIENTO/VECTOR/700'),
    ('Vector de viento y div. 850hPa','1/VIENTO/VECTOR/850'),
    ('Vector de viento y div. 500hPa','2/VIENTO/VECTOR/500'),
    ('Vector de viento y div. 700hPa','2/VIENTO/VECTOR/700'),
    ('Vector de viento y div. 850hPa','2/VIENTO/VECTOR/850'),
    ('Vector de viento y div. 500hPa','3/VIENTO/VECTOR/500'),
    ('Vector de viento y div. 700hPa','3/VIENTO/VECTOR/700'),
    ('Vector de viento y div. 850hPa','3/VIENTO/VECTOR/850')]

for nombre, ruta in Variables:
    joel = WRF.objects.filter(path__contains=ruta)
    for jo in joel:
        jo.parameter = nombre
        jo.save()
        print(nombre)
    del joel

Tiempos = ['03','06','09','12','15','18','21','24','27','30',
    '33','36','39','42','45','48']

for tiempo in Tiempos:
    wrfs_path = WRF.objects.filter(path__contains=tiempo)
    for wrf in wrfs_path:
        wrf.time = int(tiempo)
        wrf.save()
        print(tiempo)
