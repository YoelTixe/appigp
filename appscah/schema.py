import graphene

from graphene import relay, ObjectType
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from appscah.models import Category, WRF


# Graphene will automatically map the Category model's fields onto the CategoryNode.
# This is configured in the CategoryNode's Meta class (as you can see below)
#class CategoryNode(DjangoObjectType):
#    class Meta:
#        model = Category
#        filter_fields = {
#            'name': ['contains'],
#            }
#        interfaces = (relay.Node, )

#class WRFNode(DjangoObjectType):
#    class Meta:
#        model = WRF
#        filter_fields = {
#            'domain': ['exact','icontains'],
#            'parameter': ['exact'],
#            'time': ['exact'],
#            }
#        interfaces = (relay.Node, )

class WRFType(DjangoObjectType):
    class Meta:
        model = WRF
        fields = ("domain", "parameter", "time", "photo")

class Query(graphene.ObjectType):
    all_wrf = graphene.List(WRFType, 
        domain=graphene.String(required=True),
        parameter=graphene.String(required=True))
        #time=graphene.Int(required=True))
    #category_by_name = graphene.Field(CategoryType, name=graphene.String(required=True))

    def resolve_all_wrf(root, info, domain, parameter):
        lista = WRF.objects.filter(domain__contains=domain,parameter__contains=parameter,time__gte=0)
        # We can easily optimize query count in the resolve method
        #return Ingredient.objects.select_related("category").all()
        return lista
    #def resolve_category_by_name(root, info, name):
    #    try:
    #        return Category.objects.get(name=name)
    #    except Category.DoesNotExist:
    #        return None

#class Query(graphene.ObjectType):
    #category = relay.Node.Field(CategoryNode)
    #all_categories = DjangoFilterConnectionField(CategoryNode)

#    wrf = relay.Node.Field(WRFNode)
#    all_wrf = DjangoFilterConnectionField(WRFNode)

###########################################################################
###########################################################################
class CategoryType(DjangoObjectType):
    class Meta:
        model = Category
#class CreateAttentionGeneral(graphene.Mutation):
class ConsultCategoryGeneral(graphene.Mutation):
    #user = graphene.Field(UserType2)
    #attgeneral = graphene.Field(CategoryType)
    consulta = graphene.Field(CategoryType)
    #consulta = DjangoFilterConnectionField(CategoryNode)
    class Arguments:
        #clinic  = graphene.Int()
        #history = graphene.Int()
        #text    = graphene.String()
        categoria = graphene.String()
        #dominio   = graphene.String()
        #variable  = graphene.String()
    #def mutate(self, info, clinic, history, text):
    def mutate(self, info, categoria):#, dominio, variable):
        #user = info.context.user
        #if user.is_anonymous:
        #    raise Exception('Necesitas login')
        #myattentions    = AttentionGeneral.objects.filter(history__clinic__positions=user.dentist)
        mycategoria     = Category.objects.filter(name__contains=categoria)
        #date_from       = datetime.datetime.now() - datetime.timedelta(days=1)
        #attention_reg   = myattentions.filter(creado__gte=date_from).count()
        #attention_perm  = user.dentist.member.atenciones
        #print(attention_reg)
        print(mycategoria)
        #if (attention_reg < attention_perm) is False:
        #    raise Exception('Ya no puedes registrar mas Atenciones, aumenta tu membresia')
        #myclinic = Clinic.objects.filter(pk=clinic, positions=user.dentist).exclude(position__job="Postulante").first()
        #print(myclinic)
        #if myclinic is None:
        #    raise Exception('Esta no es tu clinica o no existe')
        #myhistory = History.objects.filter(clinic=myclinic, pk=history).first()
        #if myhistory is None:
        #    raise Exception('La historia no existe')
        #new_att_gen = AttentionGeneral(history=myhistory,text=text)
        #new_att_gen.save()
        return ConsultCategoryGeneral(
            consulta=mycategoria
        )

class Mutation(graphene.ObjectType):
    consult_Category_General = ConsultCategoryGeneral.Field()
###########################################################################
###########################################################################