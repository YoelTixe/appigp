Aplicacion Web del IGP

Servidor: Debian Version 10

- Python 3.9
- django 3.1.4

Extenciones python
- pip install Django
- pip install psycopg2
- pip install psycopg2-binary
- pip install django-bootstrap4
- pip install "graphene>=2.0"
- pip install "graphene-django>=2.0"
- pip install django-filter
- pip install django-cors-headers
- pip install --upgrade Pillow

query {
  allWrf(domain:"Peru",parameter:"Humedad",time:3){
    path
  }
}